#!/usr/bin/env python
# -*- coding: utf-8 -*-

import praw, time, toml, sys

def authenticate(login, password, useragent):
    """Authenticate reddit bot user and return the user object.

    Keyword Arguments:
    login -- user login string
    password -- user password string
    useragent -- bot useragent string
    """

    try:
        moduser = praw.Reddit(user_agent=useragent)
        moduser.login(username, password)
        return moduser
    except praw.errors.InvalidUserPass:
        print('Incorrect username or password.')
        sys.exit(-1)
    except praw.errors.RateLimitExceeded:
        print('Exceeding API rate limit.')
        sys.exit(-1)

def openconfig(conf='./config.toml'):
    """Read toml config file and return a dictionary with the configuration data.

    Keyword Arguments:
    conf -- configuration file location (default './config.toml')
    """

    try:
        with open(conf) as conffile:
            config = toml.loads(conffile.read())
            return config
    except IOError:
        print('Configuration file not found.')
        sys.exit(-1)

def modloop(moduser, subreddit, filter_words):
    """Performs moderation actions. Should be called in main() in an endless loop.

    Keyword Arguments:
    moduser -- authenticated reddit user object
    subreddit -- the subreddit object
    filter_words -- list of filter words to apply 
    """

    try:
        for submission in subreddit.get_new(limit=10):
            post_text = submission.selftext.lower()
            is_markable = any(string in post_text for string in filter_words)
            if is_markable:
                moduser.set_flair(subreddit.display_name, submission, 'MARKED', 'marked')
                print('Setting flair on submission: \''+submission.title+'\' by '+submission.author.name)
    except praw.errors.APIException:
        print('Unable to set flair.')

def main():
    config = openconfig('./config.toml')
    moduser = authenticate(config['auth']['login'], config['auth']['password'], config['auth']['useragent'])
    subreddit = moduser.get_subreddit(config['moderation']['subreddit'])
    filter_words = config['moderation']['filter_words']
    interval = config['moderation']['interval']
    
    while True:
        modloop(moduser, subreddit, filter_words)
        time.sleep(interval * 60)
            
if __name__ == "__main__":
    main()
